<?php

namespace App\Traits;

use GuzzleHttp\Client as Guzzle;

trait GuzzleTrait
{
    public function makeRequest($method = "GET", $url, $options = [])
    {
        $client   = new Guzzle();
        $response = $client->request($method, $url, $options);

        $results  = $response->getBody();
        
        return json_decode($results, true);
    }
}