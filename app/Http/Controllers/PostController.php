<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\GuzzleTrait;
use Log;

class PostController extends Controller
{
    use GuzzleTrait;

    /**
     * Class constructor
     */
    public function __construct()
    {
        
    }

    /**
     * Get top commented post
     */
    public function getTopPost()
    {
        $posts    = $this->makeRequest('GET', 'https://jsonplaceholder.typicode.com/posts');
        $comments = $this->makeRequest('GET', 'https://jsonplaceholder.typicode.com/comments');
        $comments = collect($comments)->map(function($comment) {
            return (object) $comment;
        });

        $reconstructedPosts = [];

        foreach($posts as $key => $post) {
            $post['total_comments'] = $comments->where('postId', $post['id'])->count();
            $reconstructedPosts[$key] = $post;
        }

        $reconstructedPosts = collect($reconstructedPosts)->map(function($post) {
            return (object) $post;
        });

        $descPosts = $reconstructedPosts->sortByDesc('total_comments');

        return response()->json([
            'status'   => '200',
            'msessage' => 'Posts sorted',
            'posts'    => $descPosts, 
        ]);
    }

    /**
     * Filter posts
     */
    public function filterPosts(Request $request)
    {
        $filters = $request->all();

        $posts = $this->makeRequest('GET', 'https://jsonplaceholder.typicode.com/posts');
        $posts = collect($posts)->map(function($post) {
            return (object) $post;
        });

        foreach($filters as $filter => $value) {
            $posts = $posts->where($filter, $value);
        }

        return response()->json([
            'status'  => '200',
            'message' => 'Posts filters',
            'posts'   => $posts,
        ]);
    }
}
